from devices_controller import __INIT__
from flask import Flask
from flask_cors import CORS


# Instance of Flask
app = Flask(__name__)
# Configuration CORS.
CORS(app,
     resources={r"/*": {"origins": "*"}},
     #headers=['Content-Type', 'X-Requested-With', 'Authorization']
     )

if __name__ == '__main__':
    app.run(debug=True, port=8000)

__INIT__(app)
