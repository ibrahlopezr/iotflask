"""
Flask App in IIS Server - IoT API Radical.
"""
from env import ASSET_ID, ENDPOINT, ACCESS_ID, ACCESS_KEY, IMAGE_ENDPOINT, USERNAME, PASSWORD
from tuya_iot import (
    TuyaAssetManager,
    TuyaOpenAPI,
    AuthType,
    TuyaOpenMQ
)
from flask import Flask, jsonify, request, send_file


# Init
def __INIT__(app):
    openapi = TuyaOpenAPI(ENDPOINT, ACCESS_ID, ACCESS_KEY, AuthType.CUSTOM)
    openapi.connect(USERNAME, PASSWORD)
    openmq = TuyaOpenMQ(openapi)
    openmq.start()

    @app.route('/changeStatus', methods=['POST'])
    def changeStatus():
        
        commands = request.get_json()
        DEVICE_ID = request.args.get('DEVICE_ID')

        print('commands: {}'.format(commands))

        data = False
        text = ''
        try:
            if commands is not None:
                result = openapi.post(
                    '/v1.0/iot-03/devices/{}/commands'.format(DEVICE_ID), commands)
                print('result: {}'.format(result))
                data = result['success']
                if result['success'] == False:
                    text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})

    @app.route('/turn-on-off', methods=['GET'])
    def turnOnOff():
        
        action = request.args.get('action')
        DEVICE_ID = request.args.get('DEVICE_ID')

        text = ''
        data = False
        try:
            if action is not None:
                # Definimos la accion a realizar.
                flag = True if action == 'on' else False


                commands = {'commands': [
                    {'code': 'switch_led', 'value': flag}]}
                result = openapi.post(
                    '/v1.0/iot-03/devices/{}/commands'.format(DEVICE_ID), commands)
                print('result: {}'.format(result))
                data = result['success']
                if result['success'] == False:
                    text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': False})

        return jsonify({"message": text,  'data': data})

    @app.route('/devices', methods=['GET'])
    def devices():
        
        text = ''
        data = False
        try:
            assetManager = TuyaAssetManager(openapi)
            devIds = assetManager.get_device_list(ASSET_ID)
            ids = iterateIdDevices(devIds)

            response = openapi.get('/v1.0/iot-03/devices', {'device_ids': ids})

            print('response: {}'.format(response))

            for item in response['result']['list']:
                item['icon'] = IMAGE_ENDPOINT + item['icon']
            # Verificamos el resultado.
            data = response['result']
            text = ''

            # if result['success'] == False:

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})

    def iterateIdDevices(devIds):
        i = 0
        ids = ''
        for id in devIds:
            if i == len(devIds):
                ids += id
            else:
                ids += id + ','
            i = i + 1

        return ids

    @app.route('/status', methods=['GET'])
    def status():
        
        DEVICE_ID = request.args.get('DEVICE_ID')
        text = ''
        data = any
        try:
            result = openapi.get(
                '/v1.0/iot-03/devices/{}/status'.format(DEVICE_ID))
            print('result: {}'.format(result))

        # Verificamos el resultado.
            data = result['result']

            if result['success'] == False:
                text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})

    @app.route('/turn-off-devices', methods=['POST'])
    def turn_off_devices():
        
        devices = request.get_json()
        print('devices: {}'.format(devices))
        text = ''
        data = False
        result = any
        try:
            if devices is not None:

                for id in devices:
                    commands = {
                        'commands':
                        [
                            {
                                'code': 'switch_led',
                                'value': False
                            }
                        ]
                    }
                    result = openapi.post(
                        '/v1.0/iot-03/devices/{}/commands'.format(id), commands)
                    print('result: {}'.format(result))

                # Verificamos el resultado.
                data = result['success']
                if result['success'] == False:
                    text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})

    @app.route('/turn-on-devices', methods=['POST'])
    def turn_on_devices():
        
        devices = request.get_json()
        print('devices: {}'.format(devices))
        text = ''
        data = False
        result = any
        try:
            if devices is not None:

                for id in devices:
                    commands = {
                        'commands':
                        [
                            {
                                'code': 'switch_led',
                                'value': True
                            }
                        ]
                    }
                    result = openapi.post(
                        '/v1.0/iot-03/devices/{}/commands'.format(id), commands)
                    print('result: {}'.format(result))

                # Verificamos el resultado.
                data = result['success']
                if result['success'] == False:
                    text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})

    @app.route('/checkstatus', methods=['GET'])
    def checkstatus():
        
        deviceId = request.args.get('id')
        print('deviceId: {}'.format(deviceId))
        text = ''
        data = False
        result = any
        try:
            result = openapi.get('/v1.0/iot-03/devices/{}/status'.format(deviceId))
            print('result: {}'.format(result))
            data = result['result']

            if result['success'] == False:
                text = result['msg']

        except ValueError:
            return jsonify({'message': text, 'data': data})

        return jsonify({"message": text,  'data': data})
